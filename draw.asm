.186
.model tiny
.code

public fill_background
public draw_frame
public draw_line
public new_line
public draw_exit
public delay

locals @@

;==============================================================================
; In:
; 	AX    - background symbol and attribute
; 	ES:DI - memory ptr
;	
;==============================================================================

fill_background proc
		push cx 

		mov cx, (80d * 20d)*2d
		rep stosw

		pop cx 

		endp


;==============================================================================
; draws_line and move next line
; In:
;	AL - horizontal symbol
;   AH - attribute
;	BL - beginning 
; 	BH - end
; 	CX - number of filling symbols in line
; 	ES:DI - memory ptr
;==============================================================================

draw_line proc
		push cx

		mov byte ptr es:[di], BL
		mov byte ptr es:[di + 1], AH
		add di, 2

		rep stosw
		
		mov byte ptr es:[di], BH
		mov byte ptr es:[di + 1], AH
		add di, 2

		pop cx


		ret 
		endp


;==============================================================================
; In:
;	CX - width
;   DX - height
; 	ES:DI - mem point
;==============================================================================

		TLC equ 201d
		TRC equ 187d
		BLC equ 200d
		BRC equ 188d
		WSP equ  32d
		HOR equ 205d
		VER equ 186d

draw_frame proc
		mov ah, 4eh
		mov al, HOR
		mov bl, TLC
		mov bh, TRC 

		call draw_line

		mov byte ptr es:[di], WSP 
		mov byte ptr es:[di + 1], 0d		
		
		mov al, WSP
		mov bl, VER 
		mov bh, VER 

@@body:
		call new_line
		call draw_line	
		call delay

		mov byte ptr es:[di], WSP 
		mov byte ptr es:[di + 1], 0d		

		dec dx
		cmp dx, 0

		jne @@body

		
		mov al, HOR
		mov bl, BLC
		mov bh, BRC 

		mov byte ptr es:[di], WSP 
		mov byte ptr es:[di + 1], 0d		

		call new_line
		call draw_line

		mov byte ptr es:[di], WSP 
		mov byte ptr es:[di + 1], 0d		

		call new_line

		add di, 2
		dec cx

		mov al, WSP
		mov ah, 0d
		inc cx
		call draw_line

		ret 
		endp

	
;==============================================================================
; moves mem point to the new line
; In:
; 	ES:DI - mem point
;==============================================================================
new_line proc

		shl cx, 1	
		sub di, cx
		sub di, 4
		add di, 160
		shr cx, 1

		ret
		endp


delay proc
		pusha

		mov cx, 03h
		mov dx, 4240h	
		mov ah, 86h
		int 15h

		popa	
		ret
		endp


draw_exit:
end
