.186
.model tiny
.code
locals @@

extrn draw_exit

extrn fill_background: proc
extrn      draw_frame: proc
extrn       draw_line: proc
extrn        new_line: proc
extrn           delay: proc

org 100h
Start:
		call show_window
		call show_text

		xor ax, ax
		mov es, ax


	;saving old interrupt
		mov bx, 9h * 4h
		mov ax, word ptr es:[bx]
		mov word ptr old_09h_o, ax
		mov ax, word ptr es:[bx + 2]
		mov word ptr old_09h_s, ax

	;set new 09h allowing no hardware interrupts while changing
		cli
		mov ax, 2509h
		mov dx, offset new_09h
		int 21h

		sti

	;staying resident in memory
		mov ax, 3100h
		mov dx, offset draw_exit
		shr dx, 4
		inc dx
		int 21h;

show_window proc
		push 0b800h
		pop es
		xor di, di


		mov al, 32d 
		mov ah, 10h;
		call fill_background
		
		call delay

		mov di, (80d * 5d + 8d) * 2d
		mov cx, 60
		mov dx, 10
		call draw_frame


		ret
		endp


new_09h proc
		pusha
		push es ds
		call show_bottom_text
		pop ds es
		popa

		db 0eah
old_09h_o:
		dw 0h
old_09h_s:
		dw 0h

		endp


show_text proc
		;mov ah, 02h	
		;mov dl, 33d
		;int 21h


		mov ax, 0b800h
		mov es, ax

		mov ax, cs
		mov ds, ax

		mov di, (80d * 10d + 16d) * 2d
		mov si, offset quote_text
		mov cx, 46d
@@next:
		movsb
		mov byte ptr es:[di], 4eh
		inc di

		loop @@next

		ret
		endp

		
show_bottom_text proc
		;mov ah, 02h	
		;mov dl, 33d
		;int 21h


		mov ax, 0b800h
		mov es, ax

		mov ax, cs
		mov ds, ax

		mov di, (80d * 12d + 16d) * 2d
		mov si, offset bottom_text 
		mov cx, 46d
		

@@next:
		movsb
		mov byte ptr es:[di], 4eh
		inc di

		loop @@next

		ret
		endp


quote_text: 
		db '�� ������ ���� � ������ ������.  �. �. ��⨭.' 

bottom_text:
		db '�� ࠬ�� ��⠥��� ������������ ᫨誮� �����.'
end Start
